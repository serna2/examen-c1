/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
  h
 */

/**
 *
 * @author danie
 */
public class Administrativo extends Empleado{
    
    private int tipoContrato;

    public Administrativo(int tipoContrato, int numEmpleado, String Nombre, String Domicilio, float pagoDia, int diasTrabajados, float Impuesto) {
        super(numEmpleado, Nombre, Domicilio, pagoDia, diasTrabajados, Impuesto);
        this.tipoContrato = tipoContrato;
    }

    public Administrativo(int tipoContrato) {
        super();
        this.tipoContrato = 0;
    }

    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    @Override
    public float calcularTotal() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float calcularDescuento() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
