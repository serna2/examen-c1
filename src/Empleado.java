/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author danie
 */
public abstract class Empleado {
    
    protected int numEmpleado;
    protected String Nombre;
    protected String Domicilio;
    protected float pagoDia;
    protected int diasTrabajados;
    protected float Impuesto;

    public Empleado(int numEmpleado, String Nombre, String Domicilio, float pagoDia, int diasTrabajados, float Impuesto) {
        this.numEmpleado = numEmpleado;
        this.Nombre = Nombre;
        this.Domicilio = Domicilio;
        this.pagoDia = pagoDia;
        this.diasTrabajados = diasTrabajados;
        this.Impuesto = Impuesto;
    }
    
    public Empleado() {
        this.numEmpleado = 0;
        this.Nombre = "";
        this.Domicilio = "";
        this.pagoDia = (float) 0.0;
        this.diasTrabajados = 0;
        this.Impuesto = (float) 0.0;
    }

    public int getNumEmpleado() {
        return numEmpleado;
    }

    public void setNumEmpleado(int numEmpleado) {
        this.numEmpleado = numEmpleado;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getDomicilio() {
        return Domicilio;
    }

    public void setDomicilio(String Domicilio) {
        this.Domicilio = Domicilio;
    }

    public float getPagoDia() {
        return pagoDia;
    }

    public void setPagoDia(float pagoDia) {
        this.pagoDia = pagoDia;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }

    public float getImpuesto() {
        return Impuesto;
    }

    public void setImpuesto(float Impuesto) {
        this.Impuesto = Impuesto;
    }  
    
    public abstract float calcularTotal();
    public abstract float calcularDescuento();
}